import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Home, Login, PaySuccess, Profile, Registrasi, ScanQR, Splash, TopUp, TopUpSuccess, TransactionHistory, Transfer, TransferSuccess, TransferVerivied } from '../pages';
import { BottomNavigator } from '../components/';
import QRPayVerivied from '../pages/QRPayVerivied';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
    return (
        <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
            <Tab.Screen name="Home" component={Home} />
            <Tab.Screen name="TransactionHistory" component={TransactionHistory} />
            <Tab.Screen name="Profile" component={Profile} />
        </Tab.Navigator>
    )
}

const Router = () => {
    return (
        <Stack.Navigator initialRouteName="Splash">
            <Stack.Screen name="Splash" component={Splash} options={{ headerShown: false }} />
            <Stack.Screen name="MainApp" component={MainApp} options={{ headerShown: false }} />
            <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
            <Stack.Screen name="PaySuccess" component={PaySuccess} options={{ headerShown: false }} />
            <Stack.Screen name="Registrasi" component={Registrasi} options={{ headerShown: false }} />
            <Stack.Screen name="TopUp" component={TopUp} options={{ headerShown: false }} />
            <Stack.Screen name="TopUpSuccess" component={TopUpSuccess} options={{ headerShown: false }} />
            <Stack.Screen name="ScanQR" component={ScanQR}/>
            <Stack.Screen name="Konfirmasi Bayar" component={QRPayVerivied} />
            <Stack.Screen name="TransactionHistory" component={TransactionHistory} options={{ headerShown: false }} />
            <Stack.Screen name="Transfer" component={Transfer} options={{ headerShown: false }} />
            <Stack.Screen name="TransferSuccess" component={TransferSuccess} options={{ headerShown: false }} />
            <Stack.Screen name="TransferVerivied" component={TransferVerivied} options={{ headerShown: false }} />
        </Stack.Navigator>
    )
}

export default Router

const styles = StyleSheet.create({})
