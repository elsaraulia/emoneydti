import React from 'react'
import { StyleSheet, Text, TouchableOpacity, Image } from 'react-native'
import {Home, Profile, Transaction, TransactionActive, HomeActive, ProfileActive} from '../../assets'

const TabItem = ({ isFocused, onPress, onLongPress, label }) => {
    const Icon = () => {
        if(label === "Home") return isFocused ? <HomeActive/> : <Home />
        if(label === "Transaction") return isFocused ? <TransactionActive/> : <Transaction />
        if(label === "Profile") return isFocused ? <ProfileActive/> : <Profile />
    }
    return (
        <TouchableOpacity
            onPress={onPress}
            onLongPress={onLongPress}
            style={{ flex: 1 }}>
            <Image source={require('../../assets/icons/Home.svg')}/>
            <Text style={{ color: isFocused ? '#673ab7' : '#222' }}>
                {label}
            </Text>
        </TouchableOpacity>
    )
}

export default TabItem

const styles = StyleSheet.create({})
