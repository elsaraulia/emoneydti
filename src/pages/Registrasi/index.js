import React from 'react'
import { StyleSheet, Text, View, TextInput, Button, Image, onPress } from 'react-native'

function Registrasi({ navigation }) {
    return (
        <View
            style={{
                flex: 1,
                backgroundColor:'white'
            }}
        >
            <View
                style={{
                    justifyContent: "center",
                    marginTop: 40,
                    alignItems: "center"
                }}
            >
                <Image
                    style={{
                        width: 250,
                        height: 180,
                        marginBottom: 8, //kalau di css margin-bottom -> jsx marginBottom
                    }}
                    source={require('../../assets/images/Logo.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                />
                <Text style={{ textAlign: "center", fontSize: 18, marginBottom: 8 }}>Sign Up</Text>
            </View>
            <View
                style={{
                    justifyContent: "center",
                    marginHorizontal: 8
                }}
            >
                <TextInput
                    placeholder="Email"
                    style={{
                        borderBottomWidth: 1,
                        marginBottom: 8
                    }}
                />
                <TextInput
                    placeholder="Password"
                    style={{
                        borderBottomWidth: 1,
                        marginBottom: 8
                    }}
                />
                <TextInput
                    placeholder="Nama"
                    style={{
                        borderBottomWidth: 1,
                        marginBottom: 8
                    }}
                />
                <TextInput
                    placeholder="No. Handphone"
                    style={{
                        borderBottomWidth: 1,
                        marginBottom: 8
                    }}
                />
                <Button
                    title="SUBMIT"
                    onPress={() => navigation.navigate('Login')}
                />
                <Text style={{ textAlign: "center", marginTop: 8 }}>Telkom Digital Talent 2020</Text>
            </View>
        </View>
    );
}

function HomeScreen({ navigation }) {
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text>Home Screen</Text>
            <Button
                title="Go to Details"
                onPress={() => navigation.navigate('Details')}
            />
        </View>
    );
}


export default Registrasi

const styles = StyleSheet.create({})
