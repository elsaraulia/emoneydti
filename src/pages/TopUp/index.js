import React from 'react'
import { StyleSheet, Text, View, TextInput, Button, Image, onPress } from 'react-native'

function TopUp({ navigation }) {
    return (
        <View
            style={{
                flex: 1,
                backgroundColor:'#F4F4F4'
            }}
        >
            <View
                style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                }}
            >
                <Text style={{ textAlign: "center", fontSize: 18, marginBottom: 10 }}>Top Up</Text>
                <Image
                    style={{
                        width: 250,
                        height: 150,
                        marginBottom: 8, //kalau di css margin-bottom -> jsx marginBottom
                    }}
                    source={require('../../assets/images/TopUp.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                />
            </View>
            <View
                style={{
                    flex: 1,
                    marginHorizontal: 8
                }}
            >
                <TextInput
                    placeholder="Nominal Top Up"
                    style={{
                        borderWidth: 1,
                        borderColor:'#C4C4C4',
                        marginBottom: 20,
                        backgroundColor: 'white'
                    }}
                />
                <Button
                    title="SUBMIT"
                    onPress={() => navigation.navigate('TopUpSuccess')}
                />
                <Text style={{ justifyContent:"flex-end" ,textAlign: "center", marginTop: 80 }}>Telkom Digital Talent 2020</Text>
            </View>
        </View>
    );
}

export default TopUp

const styles = StyleSheet.create({})
