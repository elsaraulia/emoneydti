import React from 'react'
import { StyleSheet, Text, View, TextInput, Button, Image, onPress } from 'react-native'

function Login({ navigation }) {
    return (
        <View
            style={{
                flex: 1,
                backgroundColor:'white'
            }}
        >
            <View
                style={{
                    flex: 1,
                    justifyContent: "flex-end",
                    alignItems: "center"
                }}
            >
                <Image
                    style={{
                        width: 250,
                        height: 180,
                        marginBottom: 8, //kalau di css margin-bottom -> jsx marginBottom
                    }}
                    source={require('../../assets/images/Logo.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                />
                <Text style={{ textAlign: "center", fontSize: 18, marginBottom: 8 }}>Login</Text>
            </View>
            <View
                style={{
                    flex: 1,
                    marginHorizontal: 8
                }}
            >
                <TextInput
                    placeholder="Username"
                    style={{
                        borderBottomWidth: 1,
                        marginBottom: 8
                    }}
                />
                <TextInput
                    placeholder="Password"
                    style={{
                        borderBottomWidth: 1,
                        marginBottom: 8
                    }}
                />
                <Button
                    title="LOGIN"
                    onPress={() => navigation.navigate('MainApp')}
                />
                <Text style={{ textAlign: "center", marginTop: 8, marginBottom: 8 }}>Don't have an accout?</Text>
                <Button
                    title="Sign Up"
                    onPress={() => navigation.navigate('Registrasi')}
                />
                <Text style={{ textAlign: "center", marginTop: 8 }}>Telkom Digital Talent 2020</Text>
            </View>
        </View>
    );
}

function HomeScreen({ navigation }) {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Home Screen</Text>
        <Button
          title="Go to Details"
          onPress={() => navigation.navigate('Details')}
        />
      </View>
    );
  }

export default Login

const styles = StyleSheet.create({})
