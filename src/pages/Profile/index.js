import React from 'react'
import { StyleSheet, Text, View, TextInput, Button, Image, onPress } from 'react-native'

function Profile({ navigation }) {
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: 'white'
            }}
        >
            <View
                style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: '#2296F3'
                }}
            >
                <Image
                    style={{
                        width: 100,
                        height: 100,
                        marginBottom: 8, //kalau di css margin-bottom -> jsx marginBottom
                    }}
                    source={require('../../assets/images/Profil.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                />
                <Text style={{ color: 'white', textAlign: "center", fontSize: 18, marginBottom: 8 }}>Elsa Rahmatu Aulia</Text>
                <Text style={{ color: 'white', textAlign: "center", fontSize: 18, marginBottom: 3 }}>088213363243</Text>
            </View>
            <View
                style={{
                    flex: 1,
                    marginHorizontal: 8,
                    marginTop: 30
                }}
            >
                <Button
                    title="UBAH PROFILE"
                    onPress={() => navigation.navigate('MainApp')}
                />
                <Text style={{ textAlign: "center", fontSize: 18, marginBottom: 3 }}></Text>
                <Button
                    title="GANTI PASSWORD"
                    onPress={() => navigation.navigate('Login')}
                />
                <Text style={{ textAlign: "center", fontSize: 18, marginBottom: 3 }}></Text>
                <Button
                    title="LOG OUT"
                    onPress={() => navigation.navigate('Login')}
                />
            </View>
        </View>
    );
}


export default Profile

const styles = StyleSheet.create({})
