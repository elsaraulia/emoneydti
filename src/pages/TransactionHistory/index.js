import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar, Image } from 'react-native';

const DATA = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'RP. 80.000',
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'RP. 80.000',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'RP. 80.000',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d71',
        title: 'RP. 80.000',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d76',
        title: 'RP. 80.000',
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d79',
        title: 'RP. 80.000',
    },
];

const Item = ({ title }) => (
    <View> 
    <View style={{ flexDirection: 'row', flex: 1 , backgroundColor: 'white', padding: 20, marginVertical: 8, marginHorizontal: 16,}}>
        <Image
            style={{
                width: 40,
                height: 40,
                marginLeft: 10,
                marginRight: 30, //kalau di css margin-bottom -> jsx marginBottom
            }}
            source={require('../../assets/icons/TransactionIcon.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
        />
        <View>
            <Text style={styles.title}>{title}</Text>
            <Text>Transfer ke 082213456789</Text>
        </View>
        <Text>11/11/2020</Text>
    </View>
    <View>
        
    </View>
    </View>
);

const TransactionHistory = () => {
    const renderItem = ({ item }) => (
        <Item title={item.title} />
    );

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={DATA}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        backgroundColor: 'white',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
    },
    title: {
        fontSize: 24,
        marginRight: 30
    },
});

export default TransactionHistory;