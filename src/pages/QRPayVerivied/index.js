import React from 'react'
import { StyleSheet, Text, View, TextInput, Button, Image, onPress } from 'react-native'

function QRPayVerivied({ navigation }) {
    return (
        <View
            style={{
                flex: 1,
                backgroundColor:'white'
            }}
        >
            <View
                style={{
                    flex: 1,
                    marginHorizontal: 8,
                    justifyContent: "center",
                    alignItems: "center"
                }}
            >
                <Image
                    style={{
                        width: 250,
                        height: 150,
                        marginBottom: 8, //kalau di css margin-bottom -> jsx marginBottom
                    }}
                    source={require('../../assets/images/PaySuccess.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                />
                <Text style={{ backgroundColor:'white', textAlign: "center", fontSize: 20, marginBottom: 10, borderBottomWidth: 1, borderBottomColor: '#000000' }}> Rp. 60.000</Text>
            </View>
            <View
                style={{
                    flex: 1,
                    marginHorizontal: 8
                }}
            >
                <Text style={{ color:'white', backgroundColor:'#2296F3', textAlign: "center", fontSize: 20 }}>Pembayaran Kepada:</Text>
                <Text style={{ color:'white', backgroundColor:'#2296F3', textAlign: "center", fontSize: 30 }}>DTI TELKOM University</Text>
                <Text style={{ color:'white', backgroundColor:'#2296F3',textAlign: "center", fontSize: 20, marginBottom: 20 }}> Jl. Ciparay No. 20B, Kota Bandung</Text>
                <Button
                    title="SUBMIT"
                    onPress={() => navigation.navigate('PaySuccess')}
                />
                <Text style={{ justifyContent:"flex-end" ,textAlign: "center", marginTop: 80 }}>Telkom Digital Talent 2020</Text>
            </View>
        </View>
    );
}

export default QRPayVerivied

const styles = StyleSheet.create({})
