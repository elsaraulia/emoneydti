import React from 'react'
import { StyleSheet, Text, View, TextInput, Button, Image, onPress } from 'react-native'

function PaySuccess({ navigation }) {
    return (
        <View
            style={{
                flex: 1,
                backgroundColor:'white'
            }}
        >
            <View
                style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                }}
            >
                <Image
                    style={{
                        width: 250,
                        height: 180,
                        marginTop: 50,
                        marginBottom: 8, //kalau di css margin-bottom -> jsx marginBottom
                    }}
                    source={require('../../assets/images/PaySuccess.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                />
                 <Text style={{ textAlign: "center", fontSize: 20, marginBottom: 10 }}>Pembayaran Berhasil!</Text>
                <Text style={{ textAlign: "center", fontSize: 20, marginBottom: 30 }}> Rp. 60.000</Text>
            </View>
            <View
                style={{
                    flex: 1,
                    marginHorizontal: 8
                }}
            >
                <Text style={{ color:'white', backgroundColor:'#2296F3', textAlign: "center", fontSize: 20 }}>11 November 2020</Text>
                <Text style={{ color:'white', backgroundColor:'#2296F3', textAlign: "center", fontSize: 20 }}>Penerima: DTI TELKOM University</Text>
                <Text style={{ color:'white', backgroundColor:'#2296F3',textAlign: "center", fontSize: 20, marginBottom: 30 }}> Jl. Ciparay No. 20B, Kota Bandung</Text>
                <Button
                    title="SELESAI"
                    onPress={() => navigation.navigate('MainApp')}
                />
                <Text style={{ justifyContent:"flex-end" ,textAlign: "center", marginTop: 80 }}>Telkom Digital Talent 2020</Text>
            </View>
        </View>
    );
}

export default PaySuccess

const styles = StyleSheet.create({})
