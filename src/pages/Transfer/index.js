import React from 'react'
import { StyleSheet, Text, View, TextInput, Button, Image, onPress } from 'react-native'

function Transfer({ navigation }) {
    return (
        <View
            style={{
                flex: 1,
                backgroundColor:'white'
            }}
        >
            <View
                style={{
                    flex: 1,
                    justifyContent: "flex-end",
                    alignItems: "center"
                }}
            >
                <Image
                    style={{
                        width: 250,
                        height: 180,
                        marginBottom: 8, //kalau di css margin-bottom -> jsx marginBottom
                    }}
                    source={require('../../assets/images/Logo.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                />
                <Text style={{ textAlign: "center", fontSize: 18, marginBottom: 8 }}>Login</Text>
            </View>
            <View
                style={{
                    flex: 1,
                    marginHorizontal: 8
                }}
            >
                <TextInput
                    placeholder="Nominal Transfer"
                    style={{
                        borderWidth: 1,
                        borderColor:'#C4C4C4',
                        marginBottom: 8
                    }}
                />
                <TextInput
                    placeholder="No. Handphone Penerima"
                    style={{
                        borderWidth: 1,
                        borderColor:'#C4C4C4',
                        marginBottom: 15
                    }}
                />
                <Button
                    title="PERIKSA NOMOR"
                    onPress={() => navigation.navigate('TransferVerivied')}
                />
            </View>
        </View>
    );
}

export default Transfer

const styles = StyleSheet.create({})
