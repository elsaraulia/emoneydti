import React from 'react'
import { StyleSheet, Text, View, TextInput, Button, Image, onPress } from 'react-native'
import tittle from '../TransactionHistory'

function Home({ navigation }) {
    return (
        <View>
            <View style={{ backgroundColor: 'white' }}>
                <Text style={{ textAlign: "left", marginTop: 30, marginLeft: 20 }}>Saldo Anda</Text>
                <Text style={{ textAlign: "left", fontSize: 36, marginLeft: 20, marginBottom: 30 }}>Rp. 123.456.789.000</Text>
            </View>
            <View style={{ backgroundColor: '#E5E5E5' }}>
                <Text style={{}}></Text>
                <View style={{
                    justifyContent: "center", padding: 70,
                    alignItems: "center", flexDirection: 'row', flex: 1, backgroundColor: '#2296F3', marginHorizontal: 16,
                }}>
                    <Image
                        style={{
                            width: 80,
                            height: 80,
                        }}
                        source={require('../../assets/icons/TopUpIcon.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                    />
                    <Image
                        style={{
                            width: 80,
                            height: 80,
                            marginLeft: 20, //kalau di css margin-bottom -> jsx marginBottom
                        }}
                        source={require('../../assets/icons/QRPayIcon.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                    />
                    <Image
                        style={{
                            width: 80,
                            height: 80,
                            marginLeft: 20, //kalau di css margin-bottom -> jsx marginBottom
                        }}
                        source={require('../../assets/icons/TransferIcon.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                    />
                </View>
            </View>
            <View style={{
                justifyContent: "center", flexDirection: 'row', backgroundColor: '#2296F3', marginHorizontal: 16,
            }}>
                <View style={{ marginRight: 30, }}>
                    <Button
                        title="TopUp"
                        onPress={() => navigation.navigate('TopUp')}
                    />
                </View>
                <View style={{ marginRight: 20 }}>
                    <Button
                        title="Scan QR"
                        onPress={() => navigation.navigate('ScanQR')}
                    />
                </View>
                <Button
                    title="Transfer"
                    onPress={() => navigation.navigate('Transfer')}
                />
            </View>
            <View>
                <Text style={{ textAlign: "left", marginTop: 30, marginLeft: 20 }}>Transaksi Terakhir Anda:</Text>
            </View>
            <View style={{ flexDirection: 'row', flex: 1, backgroundColor: 'white', padding: 20, marginVertical: 8, marginHorizontal: 16, }}>
                <Image
                    style={{
                        width: 40,
                        height: 40,
                        marginLeft: 10,
                        marginRight: 30, //kalau di css margin-bottom -> jsx marginBottom
                    }}
                    source={require('../../assets/icons/TransactionIcon.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                />
                <View>
                    <Text style={styles.title}>RP. 80.000</Text>
                    <Text>Transfer ke 082213456789</Text>
                </View>
                <Text>11/11/2020</Text>
            </View>
            <View style={{ flexDirection: 'row', flex: 1, backgroundColor: 'white', padding: 20, marginVertical: 8, marginHorizontal: 16, }}>
                <Image
                    style={{
                        width: 40,
                        height: 40,
                        marginLeft: 10,
                        marginRight: 30, //kalau di css margin-bottom -> jsx marginBottom
                    }}
                    source={require('../../assets/icons/TransactionIcon.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                />
                <View>
                    <Text style={styles.title}>RP. 80.000</Text>
                    <Text>Transfer ke 082213456789</Text>
                </View>
                <Text>11/11/2020</Text>
            </View>
            <View style={{ flexDirection: 'row', flex: 1, backgroundColor: 'white', padding: 20, marginVertical: 8, marginHorizontal: 16, }}>
                <Image
                    style={{
                        width: 40,
                        height: 40,
                        marginLeft: 10,
                        marginRight: 30, //kalau di css margin-bottom -> jsx marginBottom
                    }}
                    source={require('../../assets/icons/TransactionIcon.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                />
                <View>
                    <Text style={styles.title}>RP. 80.000</Text>
                    <Text>Transfer ke 082213456789</Text>
                </View>
                <Text>11/11/2020</Text>
            </View>
        </View>
    )
}

export default Home

const styles = StyleSheet.create({
    title: {
        fontSize: 24,
        marginRight: 30
    },
})
