import React from 'react'
import { StyleSheet, Text, View, TextInput, Button, Image, onPress } from 'react-native'

function ScanQR({ navigation }) {
    return (
        <View
            style={{
                flex: 1,
                backgroundColor:'white'
            }}
        >
            <View
                style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                }}
            >
                <Image
                    style={{
                        marginBottom: 20, //kalau di css margin-bottom -> jsx marginBottom
                    }}
                    source={require('../../assets/images/ScanQR.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                />
                <Button
                    title="CAPTURE"
                    onPress={() => navigation.navigate('Konfirmasi Bayar')}
                />
                <Text style={{ justifyContent:"flex-end" ,textAlign: "center", marginTop: 50 }}>Telkom Digital Talent 2020</Text>
            </View>
        </View>
    );
}

export default ScanQR

const styles = StyleSheet.create({})
