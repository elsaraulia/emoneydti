import React from 'react'
import { StyleSheet, Text, View, TextInput, Button, Image, onPress } from 'react-native'

function TopUpSuccess({ navigation }) {
    return (
        <View
            style={{
                flex: 1,
                backgroundColor:'#F4F4F4'
            }}
        >
            <View
                style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                }}
            >
                <Image
                    style={{
                        width: 250,
                        height: 150,
                        marginBottom: 8, //kalau di css margin-bottom -> jsx marginBottom
                    }}
                    source={require('../../assets/images/TopUp.png')} // pastikan di foldernya ada file logo.png 1 level dengan file App.js
                />
                <Text style={{ textAlign: "center", fontSize: 20, marginBottom: 10 }}>Top Up Berhasil! </Text>
                <Text style={{ textAlign: "center", fontSize: 20, marginBottom: 10 }}> Rp. 60.000</Text>
            </View>
            <View
                style={{
                    flex: 1,
                    marginHorizontal: 8
                }}
            >
                <Text style={{ backgroundColor:'#2296F3', textAlign: "center", fontSize: 20 }}>11 November 2020 </Text>
                <Text style={{ backgroundColor:'#2296F3',textAlign: "center", fontSize: 20, marginBottom: 10 }}> VA Mandiri</Text>
                <Button
                    title="SELESAI"
                    onPress={() => navigation.navigate('MainApp')}
                />
                <Text style={{ justifyContent:"flex-end" ,textAlign: "center", marginTop: 80 }}>Telkom Digital Talent 2020</Text>
            </View>
        </View>
    );
}

export default TopUpSuccess

const styles = StyleSheet.create({})
