import Logo from './Logo.png'
import SplashBackground from './SplashBackground.png'
import PaySuccess from './PaySuccess.png'
import ScanQR from './ScanQR.png'
import TopUp from './TopUp.png'

export {Logo, SplashBackground, PaySuccess, ScanQR, TopUp}