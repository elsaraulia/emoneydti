import Home from './Home.svg'
import Profile from './Profile.svg'
import Transaction from './Transaction.svg'
import TransactionActive from './TransactionActive.svg'
import HomeActive from './HomeActive.svg'
import ProfileActive from './ProfileActive.svg'
import QRPayIcon from './QRPayIcon.png'
import TopUpIcon from './TopUpIcon.png'
import TransactionIcon from './TransactionIcon.png'
import TransferIcon from './TransferIcon.png'

export {Home, Profile, Transaction, TransactionActive, HomeActive, ProfileActive, QRPayIcon,
    TopUpIcon, TransactionIcon, TransferIcon }